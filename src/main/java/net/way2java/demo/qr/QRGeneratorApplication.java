package net.way2java.demo.qr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QRGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(QRGeneratorApplication.class, args);
    }

}
