package net.way2java.demo.qr.controller;

import com.google.zxing.WriterException;
import net.way2java.demo.qr.util.QRCodeGenerator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.IOException;
import java.util.Base64;
import java.util.Map;

@Controller
public class MainController {

    private static final String QR_CODE_IMAGE_PATH = "./src/main/resources/static/img/QRCode.png";

    @GetMapping("/")
    public String getQRcode(Model model) {
        String bitbucket = "https://bitbucket.org/jb-tutorial/springboot-rabbitmq-sample/src/master/";
        String github = "https://github.com/say2alice/springboot-kisa-seed-pkcs5.git";

        var image = new byte[0];

        try {
            image = QRCodeGenerator.getQRCodeImage(bitbucket, 250, 250);
            QRCodeGenerator.generateQRImage(github, 250, 250, QR_CODE_IMAGE_PATH);
        } catch (WriterException | IOException e) {
            e.printStackTrace();
        }

        model.addAllAttributes(Map.of(
                "bitbucket", bitbucket,
                "github", github,
                "qrcode", Base64.getEncoder().encodeToString(image)
        ));

//        model.addAttribute("bitbucket", bitbucket);
//        model.addAttribute("github", github);
//        model.addAttribute("qrcode", Base64.getEncoder().encodeToString(image));

        return "qrcode";
    }
}
